using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform trackingTarget;

    [SerializeField]
    private float m_smoothing;
    [SerializeField]
    private float m_rotateSpeed;

    private Vector3 m_targetMoveDelta;
    private Vector3 m_targetPreviousPosition;

    // Start is called before the first frame update
    void Start()
    {
        m_targetMoveDelta = new Vector3(0,0,0);
        m_targetPreviousPosition = trackingTarget.position;
        Camera.main.transform.LookAt(trackingTarget);
    }

    // Update is called once per frame
    void Update()
    {

        m_targetMoveDelta = trackingTarget.position - m_targetPreviousPosition;
        m_targetPreviousPosition = trackingTarget.position;

        transform.position = Vector3.Lerp(transform.position, transform.position + m_targetMoveDelta, m_smoothing);

         if (Input.GetKey(KeyCode.E))
        {
            transform.LookAt(trackingTarget);
            transform.Translate(Vector3.left * Time.deltaTime * m_rotateSpeed);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.LookAt(trackingTarget);
            transform.Translate(Vector3.right * Time.deltaTime * m_rotateSpeed);
        }

        if (Input.GetKey(KeyCode.Z))
        {
            Camera.main.fieldOfView += 0.5f;
        }
        if (Input.GetKey(KeyCode.X))
        {
            Camera.main.fieldOfView -= 0.5f;
        }

    }
}
