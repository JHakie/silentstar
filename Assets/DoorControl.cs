using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorControl : MonoBehaviour
{
    private Animator m_doorAnimator;
    // Start is called before the first frame update
    void Start()
    {
        m_doorAnimator = this.transform.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            m_doorAnimator.SetBool("character_nearby", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            m_doorAnimator.SetBool("character_nearby", false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
